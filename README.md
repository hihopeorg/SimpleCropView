# SimpleCropView
**本项目是基于开源项目SimpleCropView进行ohos化的移植和开发的，可以通过项目标签以及github地址（https://github.com/igreenwood/SimpleCropView ) 追踪到原项目版本**

#### 项目介绍
 - 项目名称： SimpleCropView
 - 所属系列：ohos的第三方组件适配移植
 - 功能： SimpleCropView是一个图像裁剪库。
       它简化了裁剪图像的代码，并提供了一个易于定制的UI。
 - 项目移植状态：完成
 - 调用差异：无
 - 项目作者和维护人： hihope
 - 联系方式：hihope@hoperun.com
 - 原项目Doc地址：https://github.com/igreenwood/SimpleCropView#readme
 - 原项目基线版本：v_1.1.8   sha1:f7563f640d0fcf4df91b6b30c7cabbec5d9c559c
 - 编程语言：Java
 - 外部库依赖：

#### 效果展示
<img src="screenshot/SimpleCropView.gif" />

#### 安装教程
方法1.

1. 下载依赖库har包SimpleCropView.har。
2. 启动 DevEco Studio，将下载的har包，导入工程目录“entry->libs”下。
3. 在moudle级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下har包的引用。

```
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
	……
}
```

4. 在导入的har包上点击右键，选择“Add as Library”对包进行引用，选择需要引用的模块，并点击“OK”即引用成功。

方法2.

1. 在工程的build.gradle的allprojects中，添加HAR所在的Maven仓地址

```
repositories {
    maven {
        url 'http://106.15.92.248:8081/repository/Releases/' 
    }
}
```

2. 在应用模块的build.gradle的dependencies闭包中，添加如下代码:

```
dependencies {
    implementation 'com.isseiaoki.ohos:simplecropview:1.0.1'
}
```


#### 使用说明
#####  基本使用
CropImageView属性

| Attribute 属性           | Description 描述 |
|:---				       |:---|
| scv_crop_mode            | 裁剪模式            |
| scv_background_color     | 裁剪背景色          |
| scv_overlay_color        | 裁剪区域外背景色     |
| scv_frame_color          | 裁剪区域边框颜色     |
| scv_handle_color         | 裁剪触摸球颜色       |
| scv_guide_color          | 裁剪网格线颜色       |
| scv_guide_show_mode      | 裁剪网格线展示模式   |
| scv_handle_show_mode     | 裁剪触摸球展示模式   |
| scv_handle_size          | 裁剪触摸球大小       |
| scv_touch_padding        | 裁剪触摸边距         |
| scv_min_frame_size       | 裁剪区域最小大小     |
| scv_frame_stroke_weight  | 裁剪区域边线宽度     |
| scv_guide_stroke_weight  | 裁剪网格线宽度       |
| scv_crop_enabled         | 裁剪是否可用         |
| scv_initial_frame_scale  | 裁剪区域缩放         |
| scv_animation_enabled    | 是否启用动画         |
| scv_animation_duration   | 动画时长             |
| scv_handle_shadow_enabled| 触摸球是否有阴影             |


### 常见用法：CropImageView

#### XML
```
<com.isseiaoki.simplecropview.CropImageView
        ohos:id="$+id:cropImageView"
        ohos:height="match_content"
        ohos:width="match_parent"
        ohos:weight="1"
        ohos:padding="24vp"
        custom:scv_background_color="#1C1C1C"
        custom:scv_crop_mode="ratio_4_3"
        custom:scv_frame_color="#5DAC81"
        custom:scv_frame_stroke_weight="1vp"
        custom:scv_guide_color="#5DAC81"
        custom:scv_guide_show_mode="show_always"
        custom:scv_guide_stroke_weight="1vp"
        custom:scv_handle_color="#5DAC81"
        custom:scv_handle_show_mode="show_always"
        custom:scv_handle_size="14vp"
        custom:scv_min_frame_size="50vp"
        custom:scv_touch_padding="8vp"/>
```

#### 初始化
```
mCropView.load(mSourceUri)
                .initialFrameRect(mFrameRect)
                .useThumbnail(true)
                .execute(mLoadCallback);
```

#### 设置裁剪mode
```
mCropView.setCropMode(CropImageView.CropMode.FIT_IMAGE);
```

#### 设置图片旋转角度
```
mCropView.rotateImage(CropImageView.RotateDegrees.ROTATE_M90D);
```

#### 裁剪图片并保存
```
 private final CropCallback mCropCallback = new CropCallback() {
        @Override
        public void onSuccess(PixelMap cropped) {
            mCropView.save(cropped)
                    .compressFormat(format)
                    .execute(mSaveCallback);
        }

        @Override
        public void onError(Throwable e) {
        }
    };

mCropView.crop(mSourceUri).execute(mCropCallback);
```

#### 版本迭代
 - v1.0.1


#### 版权和许可信息

The MIT License (MIT)




