

/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.isseiaoki.simplecropview;

public interface CropImageViewAttr {
    String scv_crop_mode = "scv_crop_mode";
    String scv_background_color = "scv_background_color";
    String scv_overlay_color = "scv_overlay_color";
    String scv_frame_color = "scv_frame_color";
    String scv_handle_color = "scv_handle_color";
    String scv_guide_color = "scv_guide_color";
    String scv_guide_show_mode = "scv_guide_show_mode";
    String scv_handle_show_mode = "scv_handle_show_mode";
    String scv_handle_size = "scv_handle_size";
    String scv_touch_padding = "scv_touch_padding";
    String scv_min_frame_size = "scv_min_frame_size";
    String scv_frame_stroke_weight = "scv_frame_stroke_weight";
    String scv_guide_stroke_weight = "scv_guide_stroke_weight";
    String scv_crop_enabled = "scv_crop_enabled";
    String scv_initial_frame_scale = "scv_initial_frame_scale";
    String scv_animation_enabled = "scv_animation_enabled";
    String scv_animation_duration = "scv_animation_duration";
    String scv_handle_shadow_enabled = "scv_handle_shadow_enabled";
}
