package com.isseiaoki.simplecropview.animation;


import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;

public class ValueAnimatorV14
        implements SimpleValueAnimator, Animator.StateChangedListener,
        AnimatorValue.ValueUpdateListener {
    private static final int DEFAULT_ANIMATION_DURATION = 150;
    private AnimatorValue animator;
    private SimpleValueAnimatorListener animatorListener = new SimpleValueAnimatorListener() {
        @Override
        public void onAnimationStarted() {

        }

        @Override
        public void onAnimationUpdated(float scale) {

        }

        @Override
        public void onAnimationFinished() {

        }
    };

    public ValueAnimatorV14(int interpolator) {
        animator = new AnimatorValue();
        animator.setStateChangedListener(this);
        animator.setValueUpdateListener(this);
        animator.setCurveType(interpolator);
    }

    @Override
    public void startAnimation(long duration) {
        if (duration >= 0) {
            animator.setDuration(duration);
        } else {
            animator.setDuration(DEFAULT_ANIMATION_DURATION);
        }
        animator.start();
    }

    @Override
    public void cancelAnimation() {
        animator.cancel();
    }

    @Override
    public boolean isAnimationStarted() {
        return animator.isRunning();
    }

    @Override
    public void addAnimatorListener(SimpleValueAnimatorListener animatorListener) {
        if (animatorListener != null) this.animatorListener = animatorListener;
    }

    @Override
    public void onStart(Animator animator) {
        animatorListener.onAnimationStarted();
    }

    @Override
    public void onStop(Animator animator) {

    }

    @Override
    public void onCancel(Animator animator) {
        animatorListener.onAnimationFinished();
    }

    @Override
    public void onEnd(Animator animator) {
        animatorListener.onAnimationFinished();
    }

    @Override
    public void onPause(Animator animator) {

    }

    @Override
    public void onResume(Animator animator) {

    }

    @Override
    public void onUpdate(AnimatorValue animatorValue, float v) {
        animatorListener.onAnimationUpdated(animatorValue.INFINITE);
    }
}
