package com.isseiaoki.simplecropview;


import com.isseiaoki.simplecropview.callback.LoadCallback;
import io.reactivex.Completable;
import ohos.agp.utils.RectFloat;
import ohos.utils.net.Uri;

public class LoadRequest {

    private float initialFrameScale;
    private RectFloat initialFrameRect;
    private boolean useThumbnail;
    private CropImageView cropImageView;
    private Uri uri;

    public LoadRequest(CropImageView cropImageView, Uri uri) {
        this.cropImageView = cropImageView;
        this.uri = uri;
    }

    public LoadRequest initialFrameScale(float initialFrameScale) {
        this.initialFrameScale = initialFrameScale;
        return this;
    }

    public LoadRequest initialFrameRect(RectFloat initialFrameRect) {
        this.initialFrameRect = initialFrameRect;
        return this;
    }

    public LoadRequest useThumbnail(boolean useThumbnail) {
        this.useThumbnail = useThumbnail;
        return this;
    }

    public void execute(LoadCallback callback) {
        if (initialFrameRect == null) {
            cropImageView.setInitialFrameScale(initialFrameScale);
        }
        cropImageView.loadAsync(uri, useThumbnail, initialFrameRect, callback);
    }

    public Completable executeAsCompletable() {
        if (initialFrameRect == null) {
            cropImageView.setInitialFrameScale(initialFrameScale);
        }
        return cropImageView.loadAsCompletable(uri, useThumbnail, initialFrameRect);
    }
}
