package com.isseiaoki.simplecropview;

import com.isseiaoki.simplecropview.callback.SaveCallback;
import io.reactivex.Single;
import ohos.media.image.PixelMap;
import ohos.utils.net.Uri;

public class SaveRequest {

  private CropImageView cropImageView;
  private PixelMap image;
  private String format;
  private int compressQuality = -1;

  public SaveRequest(CropImageView cropImageView, PixelMap image) {
    this.cropImageView = cropImageView;
    this.image = image;
  }

  public SaveRequest compressFormat(String compressFormat) {
    this.format = compressFormat;
    return this;
  }

  public SaveRequest compressQuality(int compressQuality) {
    this.compressQuality = compressQuality;
    return this;
  }

  private void build() {
    if (format != null) {
      cropImageView.setCompressFormat(format);
    }
    if (compressQuality >= 0) {
      cropImageView.setCompressQuality(compressQuality);
    }
  }

  public void execute(SaveCallback callback) {
    build();
    cropImageView.saveAsync(image, callback);
  }

  public Single<Uri> executeAsSingle() {
    build();
    return cropImageView.saveAsSingle(image);
  }
}
