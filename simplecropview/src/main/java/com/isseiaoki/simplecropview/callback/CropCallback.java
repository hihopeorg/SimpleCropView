package com.isseiaoki.simplecropview.callback;

import ohos.media.image.PixelMap;

public interface CropCallback extends Callback {
  void onSuccess(PixelMap cropped);
}
