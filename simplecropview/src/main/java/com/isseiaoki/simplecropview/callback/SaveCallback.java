package com.isseiaoki.simplecropview.callback;

import ohos.utils.net.Uri;

public interface SaveCallback extends Callback {
  void onSuccess(Uri uri);
}
