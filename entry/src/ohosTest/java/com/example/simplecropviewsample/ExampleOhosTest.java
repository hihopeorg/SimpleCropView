package com.example.simplecropviewsample;

import com.isseiaoki.simplecropview.CropImageView;
import com.utils.EventHelper;
import ohos.agp.components.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ExampleOhosTest {

    private MainAbility mainAbility;

    @Before
    public void before(){
        mainAbility = EventHelper.startAbility(MainAbility.class);
        EventHelper.waitForActive(mainAbility,5);
    }

    @After
    public void tearDown(){
        EventHelper.clearAbilities();
    }

    @Test
    public void testChoosePic(){
        EventHelper.triggerClickEvent(mainAbility,mainAbility.findComponentById(ResourceTable.Id_basic_sample_button));
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        GalleryAbility galleryAbility = (GalleryAbility) EventHelper.getInstance().getCurrentTopAbility();
        EventHelper.waitForActive(galleryAbility,5);
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        ListContainer listContainer = (ListContainer) galleryAbility.findComponentById(ResourceTable.Id_gallery);
        EventHelper.triggerClickEvent(galleryAbility,listContainer.getComponentAt(0));
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        BasicAbility basicAbility = (BasicAbility) EventHelper.getInstance().getCurrentTopAbility();
        EventHelper.waitForActive(basicAbility,5);
        assertNotNull("It's not null",basicAbility.getIntent().getSequenceableParam("uri"));
    }

    @Test
    public void testMoveHandler(){
        EventHelper.triggerClickEvent(mainAbility,mainAbility.findComponentById(ResourceTable.Id_basic_sample_button));
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        GalleryAbility galleryAbility = (GalleryAbility) EventHelper.getInstance().getCurrentTopAbility();
        EventHelper.waitForActive(galleryAbility,5);
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        ListContainer listContainer = (ListContainer) galleryAbility.findComponentById(ResourceTable.Id_gallery);
        EventHelper.triggerClickEvent(galleryAbility,listContainer.getComponentAt(0));
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        BasicAbility basicAbility = (BasicAbility) EventHelper.getInstance().getCurrentTopAbility();
        EventHelper.waitForActive(basicAbility,5);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        CropImageView cropImageView = (CropImageView) basicAbility.findComponentById(ResourceTable.Id_cropImageView);
        float left = cropImageView.getFrameRect().left;
        EventHelper.inputSwipe(basicAbility,195,750,260,810,3000);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        assertNotEquals("It's not equals",left,cropImageView.getFrameRect().left,0);
    }

    @Test
    public void testMoveFrame(){
        EventHelper.triggerClickEvent(mainAbility,mainAbility.findComponentById(ResourceTable.Id_basic_sample_button));
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        GalleryAbility galleryAbility = (GalleryAbility) EventHelper.getInstance().getCurrentTopAbility();
        EventHelper.waitForActive(galleryAbility,5);
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        ListContainer listContainer = (ListContainer) galleryAbility.findComponentById(ResourceTable.Id_gallery);
        EventHelper.triggerClickEvent(galleryAbility,listContainer.getComponentAt(0));
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        BasicAbility basicAbility = (BasicAbility) EventHelper.getInstance().getCurrentTopAbility();
        EventHelper.waitForActive(basicAbility,5);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        CropImageView cropImageView = (CropImageView) basicAbility.findComponentById(ResourceTable.Id_cropImageView);
        float left = cropImageView.getFrameRect().left;
        EventHelper.inputSwipe(basicAbility,cropImageView,526,829,526,300,3000);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        assertNotEquals("It's not equals",left,cropImageView.getFrameRect().left,0);
    }

    @Test
    public void testChooseCropMode(){
        EventHelper.triggerClickEvent(mainAbility,mainAbility.findComponentById(ResourceTable.Id_basic_sample_button));
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        GalleryAbility galleryAbility = (GalleryAbility) EventHelper.getInstance().getCurrentTopAbility();
        EventHelper.waitForActive(galleryAbility,5);
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        ListContainer listContainer = (ListContainer) galleryAbility.findComponentById(ResourceTable.Id_gallery);
        EventHelper.triggerClickEvent(galleryAbility,listContainer.getComponentAt(0));
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        BasicAbility basicAbility = (BasicAbility) EventHelper.getInstance().getCurrentTopAbility();
        EventHelper.waitForActive(basicAbility,5);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        CropImageView cropImageView = (CropImageView) basicAbility.findComponentById(ResourceTable.Id_cropImageView);
        Button button = (Button) basicAbility.findComponentById(ResourceTable.Id_buttonFitImage);
        EventHelper.triggerClickEvent(basicAbility,button);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        assertEquals("It's right",cropImageView.getCropMode(), CropImageView.CropMode.FIT_IMAGE);
    }

    @Test
    public void testChooseRotate(){
        EventHelper.triggerClickEvent(mainAbility,mainAbility.findComponentById(ResourceTable.Id_basic_sample_button));
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        GalleryAbility galleryAbility = (GalleryAbility) EventHelper.getInstance().getCurrentTopAbility();
        EventHelper.waitForActive(galleryAbility,5);
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        ListContainer listContainer = (ListContainer) galleryAbility.findComponentById(ResourceTable.Id_gallery);
        EventHelper.triggerClickEvent(galleryAbility,listContainer.getComponentAt(0));
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        BasicAbility basicAbility = (BasicAbility) EventHelper.getInstance().getCurrentTopAbility();
        EventHelper.waitForActive(basicAbility,5);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        CropImageView cropImageView = (CropImageView) basicAbility.findComponentById(ResourceTable.Id_cropImageView);
        assertEquals("It's zero",0,cropImageView.getRotateDegrees());
        Button button = (Button) basicAbility.findComponentById(ResourceTable.Id_buttonRotateLeft);
        EventHelper.triggerClickEvent(basicAbility,button);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        EventHelper.triggerClickEvent(basicAbility,button);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        assertNotEquals("It's not equals",0,cropImageView.getRotateDegrees());
    }

    @Test
    public void testBasicCrop(){
        EventHelper.triggerClickEvent(mainAbility,mainAbility.findComponentById(ResourceTable.Id_basic_sample_button));
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        GalleryAbility galleryAbility = (GalleryAbility) EventHelper.getInstance().getCurrentTopAbility();
        EventHelper.waitForActive(galleryAbility,5);
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        ListContainer listContainer = (ListContainer) galleryAbility.findComponentById(ResourceTable.Id_gallery);
        EventHelper.triggerClickEvent(galleryAbility,listContainer.getComponentAt(0));
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        BasicAbility basicAbility = (BasicAbility) EventHelper.getInstance().getCurrentTopAbility();
        EventHelper.waitForActive(basicAbility,5);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        CropImageView cropImageView = (CropImageView) basicAbility.findComponentById(ResourceTable.Id_cropImageView);
        Button button = (Button) basicAbility.findComponentById(ResourceTable.Id_buttonDone);
        EventHelper.triggerClickEvent(basicAbility,button);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        ResultAbility resultAbility = (ResultAbility) EventHelper.getInstance().getCurrentTopAbility();
        assertNotNull("It's not null",resultAbility.getCropUri());
    }

    @Test
    public void testRxCrop(){
        EventHelper.triggerClickEvent(mainAbility,mainAbility.findComponentById(ResourceTable.Id_rx_sample_button));
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        GalleryAbility galleryAbility = (GalleryAbility) EventHelper.getInstance().getCurrentTopAbility();
        EventHelper.waitForActive(galleryAbility,5);
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        ListContainer listContainer = (ListContainer) galleryAbility.findComponentById(ResourceTable.Id_gallery);
        EventHelper.triggerClickEvent(galleryAbility,listContainer.getComponentAt(0));
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        RxAbility rxAbility = (RxAbility) EventHelper.getInstance().getCurrentTopAbility();
        EventHelper.waitForActive(rxAbility,5);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        CropImageView cropImageView = (CropImageView) rxAbility.findComponentById(ResourceTable.Id_cropImageView);
        Button button = (Button) rxAbility.findComponentById(ResourceTable.Id_buttonDone);
        EventHelper.triggerClickEvent(rxAbility,button);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        ResultAbility resultAbility = (ResultAbility) EventHelper.getInstance().getCurrentTopAbility();
        assertNotNull("It's not null",resultAbility.getCropUri());
    }

}