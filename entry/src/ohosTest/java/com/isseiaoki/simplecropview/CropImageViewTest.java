package com.isseiaoki.simplecropview;

import com.example.simplecropviewsample.ResourceTable;
import com.isseiaoki.simplecropview.CropImageView;
import com.isseiaoki.simplecropview.callback.LoadCallback;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.animation.Animator;
import ohos.agp.components.AttrHelper;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.utils.net.Uri;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

public class CropImageViewTest {

    @Test
    public void getImagePixelMap() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        CropImageView cropImageView = new CropImageView(context);
        try {
            ImageSource imageSource = ImageSource.create(context.getResourceManager().getResource(ResourceTable.Media_sample1),null);
            PixelMap pixelMap = imageSource.createPixelmap(null);
            cropImageView.setPixelMap(pixelMap);
            assertEquals("It's right",cropImageView.getImagePixelMap(),pixelMap);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getCircularBitmap() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        CropImageView cropImageView = new CropImageView(context);
        try {
            ImageSource imageSource = ImageSource.create(context.getResourceManager().getResource(ResourceTable.Media_sample1),null);
            PixelMap pixelMap = imageSource.createPixelmap(null);
            cropImageView.setPixelMap(pixelMap);
            assertNotNull("It's not null",cropImageView.getCircularBitmap(cropImageView.getPixelMap()));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void setCropMode() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        CropImageView cropImageView = new CropImageView(context);
        try {
            ImageSource imageSource = ImageSource.create(context.getResourceManager().getResource(ResourceTable.Media_sample1),null);
            PixelMap pixelMap = imageSource.createPixelmap(null);
            cropImageView.setPixelMap(pixelMap);
            cropImageView.setCropMode(CropImageView.CropMode.CIRCLE_SQUARE);
            assertEquals("It's right",cropImageView.getCropMode(),CropImageView.CropMode.CIRCLE_SQUARE);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void setCustomRatio() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        CropImageView cropImageView = new CropImageView(context);
        try {
            ImageSource imageSource = ImageSource.create(context.getResourceManager().getResource(ResourceTable.Media_sample1),null);
            PixelMap pixelMap = imageSource.createPixelmap(null);
            cropImageView.setPixelMap(pixelMap);
            cropImageView.setCustomRatio(50,50);
            assertEquals("It's right",cropImageView.getCustomRatio().getPointXToInt(),50);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void setOverlayColor() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        CropImageView cropImageView = new CropImageView(context);
        cropImageView.setOverlayColor(Color.BLACK.getValue());
        assertEquals("It's right",cropImageView.getOverlayColor(),Color.BLACK.getValue());
    }

    @Test
    public void setFrameColor() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        CropImageView cropImageView = new CropImageView(context);
        cropImageView.setFrameColor(Color.BLACK.getValue());
        assertEquals("It's right",cropImageView.getFrameColor(),Color.BLACK.getValue());
    }

    @Test
    public void setHandleColor() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        CropImageView cropImageView = new CropImageView(context);
        cropImageView.setHandleColor(Color.BLACK.getValue());
        assertEquals("It's right",cropImageView.getHandleColor(),Color.BLACK.getValue());
    }

    @Test
    public void setGuideColor() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        CropImageView cropImageView = new CropImageView(context);
        cropImageView.setGuideColor(Color.BLACK.getValue());
        assertEquals("It's right",cropImageView.getGuideColor(),Color.BLACK.getValue());
    }

    @Test
    public void setBackgroundColor() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        CropImageView cropImageView = new CropImageView(context);
        cropImageView.setBackgroundColor(Color.BLACK.getValue());
        assertEquals("It's right",cropImageView.getBackgroundColor(),Color.BLACK.getValue());
    }

    @Test
    public void setMinFrameSizeInDp() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        CropImageView cropImageView = new CropImageView(context);
        cropImageView.setMinFrameSizeInDp(100);
        assertEquals("It's right",cropImageView.getMineFrameSize(),100* AttrHelper.getDensity(context),0);
    }

    @Test
    public  void setMinFrameSizeInPx() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        CropImageView cropImageView = new CropImageView(context);
        cropImageView.setMinFrameSizeInPx(100);
        assertEquals("It's right",cropImageView.getMineFrameSize(),100,0);
    }

    @Test
    public  void setHandleSizeInDp() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        CropImageView cropImageView = new CropImageView(context);
        cropImageView.setHandleSizeInDp(100);
        assertEquals("It's right",cropImageView.getHandleSize(),(int)(100*AttrHelper.getDensity(context)));
    }

    @Test
    public  void setTouchPaddingInDp() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        CropImageView cropImageView = new CropImageView(context);
        cropImageView.setTouchPaddingInDp(100);
        assertEquals("It's right",cropImageView.getTouchPadding(),(int)(100*AttrHelper.getDensity(context)));
    }

    @Test
    public  void setGuideShowMode() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        CropImageView cropImageView = new CropImageView(context);
        cropImageView.setGuideShowMode(CropImageView.ShowMode.SHOW_ALWAYS);
        assertEquals("It's right",cropImageView.getGuideShowMode(),CropImageView.ShowMode.SHOW_ALWAYS);
    }

    @Test
    public  void setHandleShowMode() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        CropImageView cropImageView = new CropImageView(context);
        cropImageView.setHandleShowMode(CropImageView.ShowMode.SHOW_ALWAYS);
        assertEquals("It's right",cropImageView.getHandleShowMode(),CropImageView.ShowMode.SHOW_ALWAYS);
    }

    @Test
    public  void setFrameStrokeWeightInDp() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        CropImageView cropImageView = new CropImageView(context);
        cropImageView.setFrameStrokeWeightInDp(10);
        assertEquals("It's right",cropImageView.getFrameStrokeWeight(),10*AttrHelper.getDensity(context),0);
    }

    @Test
    public  void setGuideStrokeWeightInDp() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        CropImageView cropImageView = new CropImageView(context);
        cropImageView.setGuideStrokeWeightInDp(10);
        assertEquals("It's not null",cropImageView.getGuideStrokeWeight(),10*AttrHelper.getDensity(context),0);
    }

    @Test
    public  void setCropEnabled() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        CropImageView cropImageView = new CropImageView(context);
        cropImageView.setCropEnabled(false);
        assertFalse("It's false",cropImageView.isCropEnabled());
    }

    @Test
    public void setAnimationEnabled() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        CropImageView cropImageView = new CropImageView(context);
        cropImageView.setAnimationEnabled(true);
        assertTrue("It's true",cropImageView.isAnimationEnabled());
    }

    @Test
    public void setAnimationDuration() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        CropImageView cropImageView = new CropImageView(context);
        cropImageView.setAnimationDuration(2000);
        assertEquals("It's right",cropImageView.getAnimationDuration(),2000);
    }

    @Test
    public void setInterpolator() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        CropImageView cropImageView = new CropImageView(context);
        cropImageView.setInterpolator(Animator.CurveType.ACCELERATE_DECELERATE);
        assertEquals("It's right",cropImageView.getInterpolator(),Animator.CurveType.ACCELERATE_DECELERATE);
    }
}