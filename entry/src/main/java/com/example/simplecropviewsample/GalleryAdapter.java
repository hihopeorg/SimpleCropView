
/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.example.simplecropviewsample;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.DataAbilityHelper;
import ohos.aafwk.ability.DataAbilityRemoteException;
import ohos.agp.components.*;
import ohos.media.image.ImageSource;

import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.util.List;

public class GalleryAdapter extends BaseItemProvider {

    private List<Gallery> imageUris;
    private Ability ability;

    public GalleryAdapter(Ability ability, List<Gallery> imageUris) {
        this.imageUris = imageUris;
        this.ability = ability;
    }

    @Override
    public int getCount() {
        return imageUris.size();
    }

    @Override
    public Object getItem(int i) {
        return imageUris.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        Component cpt;
        GalleryAdapter.ViewHolder viewHolder;
        int layoutRes = ResourceTable.Layout_item_gallery_image;
        if (component == null) {
            cpt = LayoutScatter.getInstance(ability).parse(layoutRes, null, false);
            viewHolder = new GalleryAdapter.ViewHolder(cpt);
            cpt.setTag(viewHolder);
        } else {
            cpt = component;
            viewHolder = (GalleryAdapter.ViewHolder) component.getTag();
        }
        cpt.setClickedListener(component1 -> {
            if(onItemClick != null){
                onItemClick.onItemClick(i);
            }
        });
        viewHolder.item = (Gallery) getItem(i);
        DataAbilityHelper helper = DataAbilityHelper.creator(ability);
        FileDescriptor fd = null;
        try {
            fd = helper.openFile(viewHolder.item.getUri(), "r");
        } catch (DataAbilityRemoteException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        ImageSource imageSource = ImageSource.create(fd, null);
        viewHolder.image.setPixelMap(imageSource.createPixelmap(null));
        return cpt;
    }

    class ViewHolder {

        private Image image;
        private Gallery item;

        public ViewHolder(Component component) {
            image = (Image) component.findComponentById(ResourceTable.Id_image);
        }
    }

    private OnItemClick onItemClick;

    public void setOnItemClick(OnItemClick onItemClick) {
        this.onItemClick = onItemClick;
    }

    public interface OnItemClick{
        void onItemClick(int pos);
    }

}
