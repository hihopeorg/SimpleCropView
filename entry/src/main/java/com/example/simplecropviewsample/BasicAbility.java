/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.example.simplecropviewsample;

import com.isseiaoki.simplecropview.CropImageView;
import com.isseiaoki.simplecropview.callback.CropCallback;
import com.isseiaoki.simplecropview.callback.LoadCallback;
import com.isseiaoki.simplecropview.callback.SaveCallback;
import com.example.simplecropviewsample.slice.BasicAbilitySlice;
import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Component;
import ohos.agp.utils.RectFloat;
import ohos.bundle.IBundleManager;
import ohos.media.image.PixelMap;
import ohos.utils.net.Uri;

public class BasicAbility extends FractionAbility {

    public static Intent createIntent(Uri uri) {
        Intent intent = new Intent();
        intent.setParam("uri",uri);
        Operation operation = new Intent.OperationBuilder()
                .withBundleName("com.example.simplecropviewsample")
                .withAbilityName("com.example.simplecropviewsample.BasicAbility")
                .build();
        intent.setOperation(operation);
        return intent;
    }

    private static final String KEY_FRAME_RECT_LEFT = "FrameRectLeft";
    private static final String KEY_FRAME_RECT_TOP = "FrameRectTop";
    private static final String KEY_FRAME_RECT_RIGHT = "FrameRectRight";
    private static final String KEY_FRAME_RECT_BOTTOM = "FrameRectBottom";
    private static final String KEY_SOURCE_URI = "SourceUri";

    // Views ///////////////////////////////////////////////////////////////////////////////////////
    private CropImageView mCropView;
    private String format = "jpeg";
    private RectFloat mFrameRect = null;
    private Uri mSourceUri = null;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(BasicAbilitySlice.class.getName());
        super.setUIContent(ResourceTable.Layout_ability_basic);
        mCropView = (CropImageView) findComponentById(ResourceTable.Id_cropImageView);
        mCropView.setDebug(true);
        bindViews();
        if (intent.getFloatParam(KEY_FRAME_RECT_LEFT,0) != 0) {
            mFrameRect = new RectFloat(intent.getFloatParam(KEY_FRAME_RECT_LEFT,0),
                    intent.getFloatParam(KEY_FRAME_RECT_TOP,0),
                    intent.getFloatParam(KEY_FRAME_RECT_RIGHT,0),
                    intent.getFloatParam(KEY_FRAME_RECT_BOTTOM,0));
            mSourceUri = intent.getSequenceableParam(KEY_SOURCE_URI);
        }
        if (mSourceUri == null) {
            mSourceUri = intent.getSequenceableParam("uri");
            System.out.println("aoki  mSourceUri = "+mSourceUri);
        }
        mCropView.load(mSourceUri)
                .initialFrameRect(mFrameRect)
                .useThumbnail(true)
                .execute(mLoadCallback);
    }

    @Override
    protected void onForeground(Intent intent) {
        super.onForeground(intent);
        intent.setParam(KEY_FRAME_RECT_LEFT,mCropView.getActualCropRect().left);
        intent.setParam(KEY_FRAME_RECT_TOP,mCropView.getActualCropRect().top);
        intent.setParam(KEY_FRAME_RECT_RIGHT,mCropView.getActualCropRect().right);
        intent.setParam(KEY_FRAME_RECT_BOTTOM,mCropView.getActualCropRect().bottom);
        intent.setParam(KEY_SOURCE_URI, mCropView.getSourceUri());
    }

    private void bindViews() {
        mCropView = (CropImageView) findComponentById(ResourceTable.Id_cropImageView);
        findComponentById(ResourceTable.Id_buttonDone).setClickedListener(btnListener);
        findComponentById(ResourceTable.Id_buttonFitImage).setClickedListener(btnListener);
        findComponentById(ResourceTable.Id_button1_1).setClickedListener(btnListener);
        findComponentById(ResourceTable.Id_button3_4).setClickedListener(btnListener);
        findComponentById(ResourceTable.Id_button4_3).setClickedListener(btnListener);
        findComponentById(ResourceTable.Id_button9_16).setClickedListener(btnListener);
        findComponentById(ResourceTable.Id_button16_9).setClickedListener(btnListener);
        findComponentById(ResourceTable.Id_buttonFree).setClickedListener(btnListener);
        findComponentById(ResourceTable.Id_buttonPickImage).setClickedListener(btnListener);
        findComponentById(ResourceTable.Id_buttonRotateLeft).setClickedListener(btnListener);
        findComponentById(ResourceTable.Id_buttonRotateRight).setClickedListener(btnListener);
        findComponentById(ResourceTable.Id_buttonCustom).setClickedListener(btnListener);
        findComponentById(ResourceTable.Id_buttonCircle).setClickedListener(btnListener);
        findComponentById(ResourceTable.Id_buttonShowCircleButCropAsSquare).setClickedListener(btnListener);
    }

    private final LoadCallback mLoadCallback = new LoadCallback() {
        @Override public void onSuccess() {
        }

        @Override public void onError(Throwable e) {
        }
    };

    private final Component.ClickedListener btnListener = new Component.ClickedListener() {
        @Override public void onClick(Component v) {
            switch (v.getId()) {
                case ResourceTable.Id_buttonDone:
                    if (verifySelfPermission("ohos.permission.WRITE_USER_STORAGE") != IBundleManager.PERMISSION_GRANTED) {
                        if (canRequestPermission("ohos.permission.WRITE_USER_STORAGE")) {
                            requestPermissionsFromUser(
                                    new String[] { "ohos.permission.WRITE_USER_STORAGE" } , 100);
                        } else {
                        }
                    } else {
                        mCropView.crop(mSourceUri).execute(mCropCallback);
                    }
                    break;
                case ResourceTable.Id_buttonFitImage:
                    mCropView.setCropMode(CropImageView.CropMode.FIT_IMAGE);
                    break;
                case ResourceTable.Id_button1_1:
                    mCropView.setCropMode(CropImageView.CropMode.SQUARE);
                    break;
                case ResourceTable.Id_button3_4:
                    mCropView.setCropMode(CropImageView.CropMode.RATIO_3_4);
                    break;
                case ResourceTable.Id_button4_3:
                    mCropView.setCropMode(CropImageView.CropMode.RATIO_4_3);
                    break;
                case ResourceTable.Id_button9_16:
                    mCropView.setCropMode(CropImageView.CropMode.RATIO_9_16);
                    break;
                case ResourceTable.Id_button16_9:
                    mCropView.setCropMode(CropImageView.CropMode.RATIO_16_9);
                    break;
                case ResourceTable.Id_buttonCustom:
                    mCropView.setCustomRatio(7, 5);
                    break;
                case ResourceTable.Id_buttonFree:
                    mCropView.setCropMode(CropImageView.CropMode.FREE);
                    break;
                case ResourceTable.Id_buttonCircle:
                    mCropView.setCropMode(CropImageView.CropMode.CIRCLE);
                    break;
                case ResourceTable.Id_buttonShowCircleButCropAsSquare:
                    mCropView.setCropMode(CropImageView.CropMode.CIRCLE_SQUARE);
                    break;
                case ResourceTable.Id_buttonRotateLeft:
                    mCropView.rotateImage(CropImageView.RotateDegrees.ROTATE_M90D);
                    break;
                case ResourceTable.Id_buttonRotateRight:
                    mCropView.rotateImage(CropImageView.RotateDegrees.ROTATE_90D);
                    break;
                case ResourceTable.Id_buttonPickImage:
                    startAbilityForResult(GalleryAbility.createIntent(1),1);
                    break;
            }
        }
    };

    @Override
    public void onRequestPermissionsFromUserResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsFromUserResult(requestCode, permissions, grantResults);
        if (requestCode == 100){
            mCropView.crop(mSourceUri).execute(mCropCallback);
        }
    }

    @Override
    protected void onAbilityResult(int requestCode, int resultCode, Intent resultData) {
        super.onAbilityResult(requestCode, resultCode, resultData);
        if(requestCode == 1 && resultCode == 1){
            mSourceUri = resultData.getSequenceableParam("uri");
            mCropView.load(mSourceUri)
                    .initialFrameRect(mFrameRect)
                    .useThumbnail(true)
                    .execute(mLoadCallback);
        }
    }

    private final CropCallback mCropCallback = new CropCallback() {
        @Override
        public void onSuccess(PixelMap cropped) {
            mCropView.save(cropped)
                    .compressFormat(format)
                    .execute(mSaveCallback);
        }

        @Override
        public void onError(Throwable e) {
        }
    };

    private final SaveCallback mSaveCallback = new SaveCallback() {
        @Override public void onSuccess(Uri outputUri) {
            startResultActivity(outputUri);
        }

        @Override public void onError(Throwable e) {

        }
    };

    public void startResultActivity(Uri uri) {
        if (isTerminating()) return;
        startAbility(ResultAbility.createIntent( uri));
    }


}
