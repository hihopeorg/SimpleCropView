
/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.example.simplecropviewsample;

import com.example.simplecropviewsample.slice.GalleryAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.DataAbilityHelper;
import ohos.aafwk.ability.DataAbilityRemoteException;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.ListContainer;
import ohos.agp.components.TableLayoutManager;
import ohos.bundle.IBundleManager;
import ohos.data.resultset.ResultSet;
import ohos.media.photokit.metadata.AVStorage;
import ohos.utils.net.Uri;

import java.util.ArrayList;
import java.util.List;

public class GalleryAbility extends Ability {

    public static Intent createIntent(int type) {
        Intent intent = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withBundleName("com.example.simplecropviewsample")
                .withAbilityName("com.example.simplecropviewsample.GalleryAbility")
                .build();
        intent.setOperation(operation);
        intent.setParam("type",type);
        return intent;
    }

    private GalleryAdapter galleryAdapter;
    private List<Gallery> imageList = new ArrayList<>();
    private final int GALLERY_CODE = 100;
    private int type = 0;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(GalleryAbilitySlice.class.getName());
        setUIContent(ResourceTable.Layout_ability_gallery);
        type = intent.getIntParam("type",0);
        if (verifySelfPermission("ohos.permission.READ_MEDIA") != IBundleManager.PERMISSION_GRANTED) {
            if (canRequestPermission("ohos.permission.READ_MEDIA")) {
                requestPermissionsFromUser(
                        new String[] { "ohos.permission.READ_MEDIA" } , GALLERY_CODE);
            } else {
            }
        } else {
            getGalleryImages();
        }
    }

    @Override
    public void onRequestPermissionsFromUserResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsFromUserResult(requestCode, permissions, grantResults);
        if (requestCode == GALLERY_CODE){
            getGalleryImages();
        }
    }

    private void getGalleryImages(){
        DataAbilityHelper helper = DataAbilityHelper.creator(this);
        try {
            ResultSet resultSet = helper.query(AVStorage.Images.Media.EXTERNAL_DATA_ABILITY_URI, new String[]{AVStorage.Images.Media.ID}, null);
            while (resultSet != null && resultSet.goToNextRow()) {
                int id = resultSet.getInt(resultSet.getColumnIndexForName(AVStorage.Images.Media.ID));
                Uri uri = Uri.appendEncodedPathToUri(AVStorage.Images.Media.EXTERNAL_DATA_ABILITY_URI, String.valueOf(id));
                imageList.add(new Gallery(uri));
            }
            galleryAdapter = new GalleryAdapter(this,imageList);
            galleryAdapter.setOnItemClick(pos -> {
                if(type == 1){
                    startAbility(RxAbility.createIntent(imageList.get(pos).getUri()));
                }else{
                    startAbility(BasicAbility.createIntent(imageList.get(pos).getUri()));
                }
                terminateAbility();
            });
            ListContainer listContainer = (ListContainer) findComponentById(ResourceTable.Id_gallery);
            TableLayoutManager tableLayoutManager = new TableLayoutManager();
            tableLayoutManager.setColumnCount(3);
            listContainer.setLayoutManager(tableLayoutManager);
            listContainer.setItemProvider(galleryAdapter);
        } catch (DataAbilityRemoteException e) {
            e.printStackTrace();
        }
    }

}
