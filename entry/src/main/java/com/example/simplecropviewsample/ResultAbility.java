
/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.example.simplecropviewsample;

import com.example.simplecropviewsample.slice.ResultAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.DataAbilityHelper;
import ohos.aafwk.ability.DataAbilityRemoteException;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Image;
import ohos.media.image.ImageSource;
import ohos.utils.net.Uri;

import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.util.concurrent.ExecutorService;

public class ResultAbility extends Ability {

    private static final String TAG = ResultAbility.class.getSimpleName();
    private Image image;
    private ExecutorService mExecutor;
    private Uri cropUri;

    public static Intent createIntent(Uri uri) {
        Intent intent = new Intent();
        intent.setParam("uri",uri);
        Operation operation = new Intent.OperationBuilder()
                .withBundleName("com.example.simplecropviewsample")
                .withAbilityName("com.example.simplecropviewsample.ResultAbility")
                .build();
        intent.setOperation(operation);
        return intent;
    }

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(ResultAbilitySlice.class.getName());
        super.setUIContent(ResourceTable.Layout_ability_result);
        image = (Image) findComponentById(ResourceTable.Id_result_image);
        cropUri= getIntent().getSequenceableParam("uri");
        DataAbilityHelper helper = DataAbilityHelper.creator(getContext());
        try {
            FileDescriptor fd = helper.openFile(cropUri,"r");
            ImageSource imageSource = ImageSource.create(fd,null);
            image.setPixelMap(imageSource.createPixelmap(null));
        } catch (DataAbilityRemoteException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public Uri getCropUri(){
        return cropUri;
    }

}
