
/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.example.simplecropviewsample;

import com.example.simplecropviewsample.slice.RxAbilitySlice;
import com.isseiaoki.simplecropview.CropImageView;
import com.isseiaoki.simplecropview.util.Logger;
import com.isseiaoki.simplecropview.util.Utils;
import io.reactivex.CompletableSource;
import io.reactivex.SingleSource;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;
import io.reactivex.schedulers.Schedulers;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Component;
import ohos.agp.utils.RectFloat;
import ohos.bundle.IBundleManager;
import ohos.media.image.PixelMap;
import ohos.utils.net.Uri;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

public class RxAbility extends Ability {

    public static Intent createIntent(Uri uri) {
        Intent intent = new Intent();
        intent.setParam("uri", uri);
        Operation operation = new Intent.OperationBuilder()
                .withBundleName("com.example.simplecropviewsample")
                .withAbilityName("com.example.simplecropviewsample.RxAbility")
                .build();
        intent.setOperation(operation);
        return intent;
    }

    private CropImageView mCropView;
    private static final String KEY_FRAME_RECT_LEFT = "FrameRectLeft";
    private static final String KEY_FRAME_RECT_TOP = "FrameRectTop";
    private static final String KEY_FRAME_RECT_RIGHT = "FrameRectRight";
    private static final String KEY_FRAME_RECT_BOTTOM = "FrameRectBottom";
    private static final String KEY_SOURCE_URI = "SourceUri";
    private String format = "jpeg";
    private RectFloat mFrameRect = null;
    private Uri mSourceUri = null;
    private CompositeDisposable mDisposable = new CompositeDisposable();

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(RxAbilitySlice.class.getName());
        super.setUIContent(ResourceTable.Layout_ability_basic);
        mCropView = (CropImageView) findComponentById(ResourceTable.Id_cropImageView);
        bindViews();
        if (intent.getFloatParam(KEY_FRAME_RECT_LEFT, 0) != 0) {
            mFrameRect = new RectFloat(intent.getFloatParam(KEY_FRAME_RECT_LEFT, 0),
                    intent.getFloatParam(KEY_FRAME_RECT_TOP, 0),
                    intent.getFloatParam(KEY_FRAME_RECT_RIGHT, 0),
                    intent.getFloatParam(KEY_FRAME_RECT_BOTTOM, 0));
            mSourceUri = intent.getSequenceableParam(KEY_SOURCE_URI);
        }
        if (mSourceUri == null) {
            mSourceUri = intent.getSequenceableParam("uri");
            System.out.println("aoki  mSourceUri = " + mSourceUri);
        }
        mDisposable.add(loadImage(mSourceUri));
    }

    @Override
    protected void onForeground(Intent intent) {
        super.onForeground(intent);
        intent.setParam(KEY_FRAME_RECT_LEFT, mCropView.getActualCropRect().left);
        intent.setParam(KEY_FRAME_RECT_TOP, mCropView.getActualCropRect().top);
        intent.setParam(KEY_FRAME_RECT_RIGHT, mCropView.getActualCropRect().right);
        intent.setParam(KEY_FRAME_RECT_BOTTOM, mCropView.getActualCropRect().bottom);
        intent.setParam(KEY_SOURCE_URI, mCropView.getSourceUri());
    }

    private Disposable loadImage(final Uri uri) {
        mSourceUri = uri;
        return mCropView.load(uri)
                .useThumbnail(true)
                .initialFrameRect(mFrameRect)
                .executeAsCompletable()
                .subscribeOn(Schedulers.newThread())
                .subscribe(new Action() {
                    @Override
                    public void run() throws Exception {
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable throwable) throws Exception {
                    }
                });
    }

    private Disposable cropImage() {
        return mCropView.crop(mSourceUri)
                .executeAsSingle()
                .flatMap(new Function<PixelMap, SingleSource<Uri>>() {
                    @Override
                    public SingleSource<Uri> apply(@io.reactivex.annotations.NonNull PixelMap bitmap)
                            throws Exception {
                        return mCropView.save(bitmap)
                                .compressFormat(format)
                                .executeAsSingle();
                    }
                })
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(@io.reactivex.annotations.NonNull Disposable disposable)
                            throws Exception {
                    }
                })
                .doFinally(new Action() {
                    @Override
                    public void run() throws Exception {
                    }
                })
                .subscribeOn(Schedulers.newThread())
                .subscribe(new Consumer<Uri>() {
                    @Override
                    public void accept(@io.reactivex.annotations.NonNull Uri uri) throws Exception {
                        startResultActivity(uri);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@io.reactivex.annotations.NonNull Throwable throwable)
                            throws Exception {
                    }
                });
    }

    private void bindViews() {
        mCropView = (CropImageView) findComponentById(ResourceTable.Id_cropImageView);
        findComponentById(ResourceTable.Id_buttonDone).setClickedListener(btnListener);
        findComponentById(ResourceTable.Id_buttonFitImage).setClickedListener(btnListener);
        findComponentById(ResourceTable.Id_button1_1).setClickedListener(btnListener);
        findComponentById(ResourceTable.Id_button3_4).setClickedListener(btnListener);
        findComponentById(ResourceTable.Id_button4_3).setClickedListener(btnListener);
        findComponentById(ResourceTable.Id_button9_16).setClickedListener(btnListener);
        findComponentById(ResourceTable.Id_button16_9).setClickedListener(btnListener);
        findComponentById(ResourceTable.Id_buttonFree).setClickedListener(btnListener);
        findComponentById(ResourceTable.Id_buttonPickImage).setClickedListener(btnListener);
        findComponentById(ResourceTable.Id_buttonRotateLeft).setClickedListener(btnListener);
        findComponentById(ResourceTable.Id_buttonRotateRight).setClickedListener(btnListener);
        findComponentById(ResourceTable.Id_buttonCustom).setClickedListener(btnListener);
        findComponentById(ResourceTable.Id_buttonCircle).setClickedListener(btnListener);
        findComponentById(ResourceTable.Id_buttonShowCircleButCropAsSquare).setClickedListener(btnListener);
    }

    private final Component.ClickedListener btnListener = new Component.ClickedListener() {
        @Override
        public void onClick(Component v) {
            switch (v.getId()) {
                case ResourceTable.Id_buttonDone:
                    if (verifySelfPermission("ohos.permission.WRITE_USER_STORAGE") != IBundleManager.PERMISSION_GRANTED) {
                        if (canRequestPermission("ohos.permission.WRITE_USER_STORAGE")) {
                            requestPermissionsFromUser(
                                    new String[]{"ohos.permission.WRITE_USER_STORAGE"}, 100);
                        } else {
                            // 显示应用需要权限的理由，提示用户进入设置授权
                        }
                    } else {
                        mDisposable.add(cropImage());
                    }
                    break;
                case ResourceTable.Id_buttonFitImage:
                    mCropView.setCropMode(CropImageView.CropMode.FIT_IMAGE);
                    break;
                case ResourceTable.Id_button1_1:
                    mCropView.setCropMode(CropImageView.CropMode.SQUARE);
                    break;
                case ResourceTable.Id_button3_4:
                    mCropView.setCropMode(CropImageView.CropMode.RATIO_3_4);
                    break;
                case ResourceTable.Id_button4_3:
                    mCropView.setCropMode(CropImageView.CropMode.RATIO_4_3);
                    break;
                case ResourceTable.Id_button9_16:
                    mCropView.setCropMode(CropImageView.CropMode.RATIO_9_16);
                    break;
                case ResourceTable.Id_button16_9:
                    mCropView.setCropMode(CropImageView.CropMode.RATIO_16_9);
                    break;
                case ResourceTable.Id_buttonCustom:
                    mCropView.setCustomRatio(7, 5);
                    break;
                case ResourceTable.Id_buttonFree:
                    mCropView.setCropMode(CropImageView.CropMode.FREE);
                    break;
                case ResourceTable.Id_buttonCircle:
                    mCropView.setCropMode(CropImageView.CropMode.CIRCLE);
                    break;
                case ResourceTable.Id_buttonShowCircleButCropAsSquare:
                    mCropView.setCropMode(CropImageView.CropMode.CIRCLE_SQUARE);
                    break;
                case ResourceTable.Id_buttonRotateLeft:
                    mCropView.rotateImage(CropImageView.RotateDegrees.ROTATE_M90D);
                    break;
                case ResourceTable.Id_buttonRotateRight:
                    mCropView.rotateImage(CropImageView.RotateDegrees.ROTATE_90D);
                    break;
                case ResourceTable.Id_buttonPickImage:
                    startAbilityForResult(GalleryAbility.createIntent(1), 1);
                    break;
            }
        }
    };

    @Override
    protected void onAbilityResult(int requestCode, int resultCode, Intent resultData) {
        super.onAbilityResult(requestCode, resultCode, resultData);
        if (requestCode == 1 && resultCode == 1) {
            mDisposable.add(cropImage());
        }
    }

    public void startResultActivity(Uri uri) {
        if (isTerminating()) return;
        startAbility(ResultAbility.createIntent(uri));
    }

    @Override
    protected void onStop() {
        super.onStop();
        mDisposable.dispose();
    }
}
